package at;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import io.vavr.Function1;
import io.vavr.collection.Stream;
import org.junit.Test;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EnJava {

    protected ImmutableList<Integer> list = ImmutableList.of(0, 1, 2, 3, 4);

    @Test
    public void testJava8() {
        Function<Integer, Integer> addOne = nb -> {
            System.out.println(nb + " + 1");
            if (nb > 2) System.out.println("Slow");
            return nb + 1;
        };
        Predicate<Integer> isSmall = nb -> {
            System.out.println(nb + " smaller than 3 ?");
            return nb < 3;
        };
        List<Integer> result = list.stream()
                .map(addOne)
                .filter(isSmall)
                .limit(2)
                .collect(Collectors.toList());
        System.out.println(result);
    }

    @Test
    public void testGuava() {
        com.google.common.base.Function<Integer, Integer> addOne = nb -> {
            System.out.println(nb + " + 1");
            if (nb > 2) System.out.println("Slow");
            return nb + 1;
        };
        com.google.common.base.Predicate<Integer> isSmall = nb -> {
            System.out.println(nb + " smaller than 3 ?");
            return nb < 3;
        };
        Iterable<Integer> transformed = Iterables.transform(list, addOne);
        Iterable<Integer> filtered = Iterables.filter(transformed, isSmall);
        System.out.println(Iterables.limit(filtered, 2));
    }

    @Test
    public void testVavr() {
        Function1<Integer, Integer> addOne = nb -> {
            System.out.println(nb + " + 1");
            if (nb > 2) System.out.println("Slow");
            return nb + 1;
        };
        Predicate<Integer> isSmall = nb -> {
            System.out.println(nb + " smaller than 3 ?");
            return nb < 3;
        };
        List<Integer> result = Stream.ofAll(list)
                .map(addOne)
                .filter(isSmall)
                .take(2)
                .collect(Collectors.toList());
        System.out.println(result);
    }

}
